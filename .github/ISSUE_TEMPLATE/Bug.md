---
name: Bug report
about: Report a problem with gcip to help resolve it and improve
---
<!--Inspired by https://github.com/restic/restic-->

<!--

Welcome! - We kindly ask you to fill out the issue template below.

NOTE: Not filling out the issue template needs a good reason, as otherwise it
may take a lot longer to find the problem, not to mention it can take up a lot
more time which can otherwise be spent on development. Please also take the
time to help us debug the issue by collecting relevant information, even if
it doesn't seem to be relevant to you. Thanks!

Thanks for understanding, and for contributing to the project!

-->


gcip version you are using?
-----------------------------
`pip freeze | grep gcip`


What error does occur?
-------------------------------
<!--
This section should include:

 * Complete error output.
 * The gitlab-ci.py, at least the section which produces the error.
 * If the project is public, please add the URL to the project.
-->

Expected behavior
-----------------
<!--
Describe what you'd expected from gcip.
-->

Steps to reproduce the behavior
-------------------------------
<!--
The more time you spend describing an easy way to reproduce the behavior (if
this is possible), the easier it is for the project developers to fix it!
-->

Do you have any idea what may have caused this?
-----------------------------------------------



Do you have an idea how to solve the issue?
-------------------------------------------



Did gcip help you today? Did it make you happy in any way?
------------------------------------------------------------
<!--
Answering this question is not required, but if you have anything positive to share, please do so here!
Idea by Joey Hess, https://joeyh.name/blog/entry/two_holiday_stories/
-->
