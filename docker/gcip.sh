#!/bin/sh

# This script is used as start command for the GCIP Docker Image.

set -x

PIPELINE_PY="${*:-.gitlab-ci.py}"

if test -f Pipfile; then
  pipenv install --system
fi

if test -f requirements.txt; then
  pip install -r requirements.txt
fi

python "$PIPELINE_PY"
